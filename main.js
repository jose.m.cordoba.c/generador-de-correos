const patrones = [
    'primer@dominio',
    'Segundo@dominio',
    'Apellido@dominio',
    'primerSegundoApellido@dominio',
    'SegundoApellido@dominio',
    'primerApellido@dominio',
    'Apellidoprimer@dominio',
    'primer.Segundo.Apellido@dominio',
    'primer.Apellido@dominio',
    'Apellido.primer.Segundo@dominio',
    'Apellido.Segundo@dominio',
    'Apellido.primer@dominio',
    'Segundo.Apellido@dominio',
    'PSegundoApellido@dominio',
    'PsApellido@dominio',
    'Psa@dominio',
    'P.Segundo.Apellido@dominio',
    'P.s.Apellido@dominio',
    'P.s.a@dominio',
    'PApellido@dominio',
    'P.Apellido@dominio',
    'sApellido@dominio',
    's.Apellido@dominio',
    'aSegundo@dominio',
    'aprimer@dominio',
    'primer_Segundo_Apellido@dominio',
    'primer_Apellido@dominio',
    'Apellido_primer_Segundo@dominio',
    'Apellido_Segundo@dominio',
    'Apellido_primer@dominio',
    'Segundo_Apellido@dominio',
    'P_Segundo_Apellido@dominio',
    'P_s_Apellido@dominio',
    'P_s_a@dominio',
    'P_Apellido@dominio',
    's_Apellido@dominio',
    'primer-Segundo-Apellido@dominio',
    'primer-Apellido@dominio',
    'Apellido-primer-Segundo@dominio',
    'Apellido-Segundo@dominio',
    'Apellido-primer@dominio',
    'Segundo-Apellido@dominio',
    'P-Segundo-Apellido@dominio',
    'P-s-Apellido@dominio',
    'P-s-a@dominio',
    'P-Apellido@dominio',
    's-Apellido@dominio',
];

function generateMail(nombre, segundo, apellido, dominio, patron) {
    return patron.replace("P", nombre ? nombre[0] : "")
        .replace("s", segundo ? segundo[0] : "")
        .replace("a", apellido ? apellido[0] : "")
        .replace("primer", nombre)
        .replace("Segundo", segundo)
        .replace("Apellido", apellido)
        .replace("dominio", dominio);
}

function generateAllMails() {
    let nombre = $("#PrimerNombre").val();
    let segundo = $("#SegundoNombre").val();
    let apellido = $("#Apellido").val();
    let dominio = $("#Dominio").val();

    mail = '';
    for (patron in patrones) {
        mail += generateMail(nombre,
                             segundo,
                             apellido,
                             dominio,
                             patrones[patron]) + ', ';
    }

    return mail;
}

$('#frm-generar').submit(function(e){
   e.preventDefault();
});

$("#btn-generar").click(function () {
    let mail = generateAllMails();
    $("#cuerpo-resultados").val(mail);
});

$("#btn-copy").click(function () {
    let data = document.getElementById("cuerpo-resultados");
    data.select();
    data.setSelectionRange(0, 99999);
    document.execCommand("copy");
});
